// Notes: This script is part of Inventory Management

define(['angular', 'components/shared/index'], function (angular) {
	var inventoryApp = angular.module('inventoryModule', ['powerSchoolModule']);
	
	inventoryApp.controller('catalogController', function($scope, $http) {

		$scope.catalogData = [];
		
		$http.get('cataloglist_stuff.json').then(function(retJSON) {
		    retJSON.data.pop();
            $scope.catalogData = retJSON.data;
		});

	});

	inventoryApp.controller('inventoryController', function($scope, $rootScope, myGetService) {

		$scope.inventoryData = [];
		$scope.selectedTile = 'all';
		
		// sets a watch on $rootScope.selectedTile to reload the table (PS Grid Widget) when this value changes
        // this funtion also fires on page-load which loads the initial table with 'all' values
        $scope.$watch(function() { return $rootScope.selectedTile; }, function() {
            
            // the $rootScope variable holds the value of the selected tile
            if (typeof $rootScope.selectedTile === 'undefined') {
                $scope.selectedTile = 'all';
            } else {
                $scope.selectedTile = $rootScope.selectedTile;
            }
            
            // get the data from the JSON source
            myGetService.getData('inventorylist_stuff.json?src=table&tile=' + encodeURIComponent($scope.selectedTile)).then(function (retData) {
                
                // remove the empty value at the end of the JSON data
                retData.pop();
				
				retData.forEach(function(v) {
					if (v.datepurchased != "") v.datepurchased = new Date(v.datepurchased);
				});  
				console.log(retData);
                // put the data into the $scope variable which is bound to the grid widget
                $scope.inventoryData = retData;

            });
            
        }, true);

	});
	
	inventoryApp.controller('studentController', function($scope, $http) {

		$scope.inventoryData = [];
		
		// this function is called by ng-init and whenever data refresh is needed
        $scope.listInventory = function(type) {
    		$http.get('studentinventory_stuff.json?type=' + type + '&sid='+$scope.student_dcid).then(function(retJSON) {
    		    retJSON.data.pop();
				retJSON.data.forEach(function(v) {
					if (v.due_date != "") v.due_date = new Date(v.due_date);				
					if (v.return_date != "") v.return_date = new Date(v.return_date);									
				});
                $scope.inventoryData = retJSON.data;
    		});
        };

	});

	inventoryApp.controller('overlappingController', function($scope, $http) {

		$scope.overlappingData = [];
		
		$http.get('overlappingassignments_stuff.json').then(function(retJSON) {
			retJSON.data.pop();
			retJSON.data.forEach(function(v, i, a) {
				if (v.issue_date1 != "") v.issue_date1 = new Date(v.issue_date1);				
				if (v.return_date1 != "") v.return_date1 = new Date(v.return_date1);
				if (v.issue_date2 != "") v.issue_date2 = new Date(v.issue_date2);				
				if (v.return_date2 != "") v.return_date2 = new Date(v.return_date2);
			});
			$scope.overlappingData = retJSON.data;
		});        

	});	
	
	inventoryApp.controller('circulationController', function($scope, $http) {

		$scope.circulationData = [];
		
		$http.get('circulation_stuff.json').then(function(retJSON) {
		    retJSON.data.pop();
			retJSON.data.forEach(function(v) {
				if (v.issue_date != "") v.issue_date = new Date(v.issue_date);
				if (v.due_date != "") v.due_date = new Date(v.due_date);
				if (v.return_date != "") v.return_date = new Date(v.return_date);				
			});
            $scope.circulationData = retJSON.data;
		});

	});
	
	// creates a directive called 'myTiles' -- it does the following:
	// -- binds the tile data to the PS Tiles Widget
	// -- changes the value of $rootscope.selectedTile when selecting a tile
    inventoryApp.directive('myTiles', function($rootScope, myGetService) {
        return {
            priority: 100,
            require: ['pssVidget', 'pssTilesVidget'],
            
            // because every controller has its own scope, they can't access this controller's scope variables
            // for that reason, we need to pass in the $rootScope and store the selected tile in a $rootScope variable
            // the $rootScope variable can be 'watched' for changes by the controller above
            controller: ['$scope', '$rootScope', function($scope, $rootScope) {
                var ctrlData = {
                    pssTilesVidget: null
                };
                $scope.myTilesData = ctrlData;
                
                // load the tile data
                myGetService.getData('/admin/stuff/inventorylist_stuff.json?src=tiles').then(function (retData) {
                    retData.pop();
                    ctrlData.pssTilesVidget.setTiles(retData);
                });

            }],
            link: function(scope, element, attributes, requires) {
                var pssTilesVidget = requires[1];
                scope.myTilesData.pssTilesVidget = pssTilesVidget;
                
                // creates a function to set a $rootScope variable when a tile is clicked
                var filterListener = function(newvalue, oldvalue) {
                    $rootScope.selectedTile = newvalue.id;
                };
                
                // this PS Tile Widget function adds the listener to the Widget
                pssTilesVidget.addTilesFilterListener(filterListener);
            }
        };
    });

    // creates a reuseable factory (service) which will get data from the server using Angular's $http service
    // this service is used by both controllers above: myConroller and myTiles
    inventoryApp.factory('myGetService', function ($http) {
        return {
            getData: function (datafile) {
                return $http.get(datafile).then(function (result) {
                        return result.data;
                });
            }
        }
    });
    
});